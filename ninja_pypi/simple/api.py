from django.http import Http404, HttpRequest
from ninja import Router

from ninja_pypi.package.models import Package as DBPackage
from .models import Package, PackageFile, PackageFileHashes

router = Router()


@router.get('/{str:package}/')
async def simple_package(request: HttpRequest, package: str) -> Package:
    package = await DBPackage.objects.filter(name=package).afirst()
    if not package:
        raise Http404('Package not found')

    file_list = []
    async for file in package.files.all():
        file_list.append(PackageFile(
            filename=file.name,
            hashes=PackageFileHashes(sha256=file.sha256),
            url=request.build_absolute_uri(file.file.url),
            size=file.file.size,
            yanked=file.yanked or False,
            requires_python=file.requires_python,
            upload_time=file.created,
        ))

    version_set = set()
    async for version in package.versions.all():
        version_set.add(version.version)

    package_model = Package(
        name=package.name,
        meta={
            '_last-serial': 1,
            'api-version': package.api_version,
        },
        files=file_list,
        versions=list(version_set)
    )

    return package_model

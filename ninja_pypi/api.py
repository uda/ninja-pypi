from ninja import NinjaAPI
from ninja.renderers import JSONRenderer

from .simple.api import router


class PyPISimpleRenderer(JSONRenderer):
    media_type = 'application/vnd.pypi.simple.v1+json'


api = NinjaAPI(renderer=PyPISimpleRenderer())
api.add_router('/simple/', router, tags=['simple'])

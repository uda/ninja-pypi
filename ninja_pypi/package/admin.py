import hashlib

from django.contrib import admin

from .models import Package, PackageVersion, PackageFile


@admin.register(Package)
class PackageAdmin(admin.ModelAdmin):
    pass


@admin.register(PackageVersion)
class PackageVersionAdmin(admin.ModelAdmin):
    pass


@admin.register(PackageFile)
class PackageFileAdmin(admin.ModelAdmin):
    readonly_fields = ('name', 'sha256', 'size')

    def save_model(self, request, obj, form, change):
        obj.name = obj.file.name
        obj.size = obj.file.size
        obj.sha256 = hashlib.sha256(obj.file.read()).hexdigest()
        super().save_model(request, obj, form, change)

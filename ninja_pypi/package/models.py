from datetime import datetime

from django.db import models


class Package(models.Model):
    name: str = models.CharField(max_length=255, unique=True)
    api_version: str = models.CharField(max_length=16, default='1.1')

    def __str__(self):
        return self.name


class PackageVersion(models.Model):
    package: Package = models.ForeignKey(Package, on_delete=models.CASCADE, related_name='versions')
    version: str = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.package.name} - {self.version}'


class PackageFile(models.Model):
    package: Package = models.ForeignKey(Package, on_delete=models.CASCADE, related_name='files')
    name: str = models.CharField(max_length=1024)
    file = models.FileField(upload_to='packages')
    sha256: str = models.CharField(max_length=64)
    requires_python: str = models.CharField(max_length=32)
    size: int = models.IntegerField()
    yanked: str = models.TextField(default='', blank=True, null=True)
    created: datetime = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.package.name} - {self.name}'
